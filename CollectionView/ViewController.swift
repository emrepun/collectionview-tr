//
//  ViewController.swift
//  CollectionView
//
//  Created by Emre HAVAN on 26.10.2017.
//  Copyright © 2017 Pun Corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let resimler = ["1","2","3","4","5"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resimler.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! CustomCell
        
        cell.layer.cornerRadius = 50
        cell.layer.borderColor = UIColor.blue.cgColor
        cell.layer.borderWidth = 3
        
        cell.resim.image = UIImage(named: resimler[indexPath.row])
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Resim \(indexPath.row + 1) tıklandı")
    }


}

